/*eslint-env browser*/
/*globals SockJS sockethost $*/
var inhandlers = {};
var outhandlers = {};

function fixUrl() {
    var hasha = window.location.hash.split('/');
    if (hasha.length === 1) {
        document.location.hash = '#/';
    }
}

function updateh3b() {
    $("#h3b1").removeClass("active");
    $("#h3b2").removeClass("active");
    $("#h3b3").removeClass("active");
    $("#h3b4").removeClass("active");
    $("#h3b5").removeClass("active");
    $("#h3b6").removeClass("active");

    fixUrl();
    var hasha = window.location.hash.split('/');
    switch (hasha[1]) {
        case '':
            $("#h3b1").addClass("active");
            break;
        case 'games':
            $("#h3b2").addClass("active");
            break;
        case 'rules':
            $("#h3b3").addClass("active");
            break;
        case 'forum':
            $("#h3b4").addClass("active");
            break;
        case 'staff':
            $("#h3b5").addClass("active");
            break;
        case 'bans':
            $("#h3b6").addClass("active");
            break;
        default:
            break;
    }
}

function goto(hash) {
    window.location.hash = hash;
}

inhandlers.content = function inHandlerContent(response) {

    updateh3b();

    $.get(response.payload.url, function(data) {
        $("#main-content").html(data);
        console.log("Loaded page: " + response.payload.url);
    });

    return null;
};

inhandlers.register = function inHandlerContent(response) {

    $("#loginFormOK").addClass("hide");

    if(response.header.status === 404) {
        $("#registerFormErrorCode").removeClass("hide");
        $("#registerFormButton").attr("disabled", false);
    }
    
    if(response.header.status === 200){
        $('#registerModal').modal('hide');
        $("#registerFormSuccess").removeClass("hide");
        localStorage.setItem("uuid", response.payload.uuid);
        localStorage.setItem("username", response.payload.username);
        localStorage.setItem("token", response.payload.token);
        $("#h1r")[0].innerHTML = ('<img src="https://crafatar.com/avatars/' + response.payload.uuid.replace(/-/g, "") + '" width="24px"><div style="display: inline-block;vertical-align: middle;margin-left: 0.5rem;">' + response.payload.username + '</div>');
        $("#h1r").addClass("h");
    }

    return null;
};

inhandlers.login = function inHandlerContent(response) {

    $("#loginFormOK").addClass("hide");

    if(response.header.status === 401) {
        $("#loginFormErrorCredentials").removeClass("hide");
        $("#loginFormButton").attr("disabled", false);
    }
    
    if(response.header.status === 200){
        $('#loginModal').modal('hide');
        $("#loginFormSuccess").removeClass("hide");
        localStorage.setItem("uuid", response.payload.uuid);
        localStorage.setItem("username", response.payload.username);
        localStorage.setItem("token", response.payload.token);
        $("#h1r")[0].innerHTML = ('<img src="https://crafatar.com/avatars/' + response.payload.uuid.replace(/-/g, "") + '" width="24px"><div style="display: inline-block;vertical-align: middle;margin-left: 0.5rem;">' + response.payload.username + '</div>');
        $("#h1r").addClass("h");
    }

    return null;
};

outhandlers.content = function outHandlerContent() {
    sock.send(JSON.stringify({
        header: {
            action: 'content',
            status: 200
        },
        payload: {
            url: window.location.href,
            hash: window.location.hash
        }

    }));
};

outhandlers.register = function outHandlerContent() {
    sock.send(JSON.stringify({
        header: {
            action: 'register',
            status: 200
        },
        payload: {
            code: Number($("#registerFormCode").val()),
            password: $("#registerFormPassword").val()
        }

    }));
};

outhandlers.login = function outHandlerContent() {
    sock.send(JSON.stringify({
        header: {
            action: 'login',
            status: 200
        },
        payload: {
            username: $("#loginFormUsername").val(),
            password: $("#loginFormPassword").val()
        }

    }));
};

// JavaScript start.
fixUrl();
var sock = new SockJS('http://' + sockethost + '/_socket');
window.onpopstate = function () {
    outhandlers.content();
};
$("#registerFormButton").click(function () {
    $("#registerFormErrorCode").addClass("hide");
    outhandlers.register();
    $("#registerFormOK").removeClass("hide");
    $("#registerFormButton").attr("disabled", true);
});
$("#loginFormButton").click(function () {
    $("#loginFormErrorCredentials").addClass("hide");
    outhandlers.login();
    $("#loginFormOK").removeClass("hide");
    $("#loginFormButton").attr("disabled", true);
});


sock.onopen = function() {
    console.log('open');
    outhandlers.content();
};
sock.onmessage = function(e) {
    console.log('message', e.data);

    var response = JSON.parse(e.data);

    console.log(response);

    if (!inhandlers.hasOwnProperty(response.header.action)) {
        alert("Err0r! Received unknown packet from server! Please report this incident. Error code: 1.");
        window.location.reload();
        return;
    }

    var packet = inhandlers[response.header.action](response);
    if (packet !== null) {
        sock.send(JSON.stringify(packet));
    }


};
sock.onclose = function() {
    console.log('close');
    alert("Socket closed.");
    setTimeout(window.location.reload, 300000)
};